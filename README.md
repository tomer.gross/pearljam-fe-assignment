# Pearl Jam Assignment
Given a web server that supplies the historic and future shows of Pearl Jam, build a Web UI that:
(the following features list is prioritized)
1. Presents the shows.
1. Supports an RSVP to a future event. RSVPed events will be shown in a separate view.
1. Supports filtering by date range (from and to, according to documentation) and by city.
1. Supports exporting the shows to a CSV file (either all shows or selected ones).

Implement with a UI framework such as Angular, React or Vue.

You have 3 hours to complete your task.

Please send your finished assignment to solutions@planckdata.com

Your finished assignment should include -
* Your code in a rar/zip file.
* Print screen of the UI with all past and future shows since 2018 and 3 shows RSVPed.
* Example exported file in the format of your choice (with either all shows or selected ones).

# API Specification
* ***username***: user
* ***password***: pearljam-6a9e692e-8468-44eb-8a47-5d5f3ac15046

### GET https://devapi.planckdata.com/pearljam-api/shows?from=mmddyyyy&to=mmddyyyy

Returns all pearljam shows in a given date range.

from and to parameters are optional.

Example -
http://localhost:1234/shows?from=03232018&to=01202021

Response -
```json
[
        {
            "id": "12324",
            "name": "Greatest show ev,m, ?er",
            "slug": "greatest-show-ever",
            "date": "2018-09-04",
            "venue_name": "The Best Park",
            "lat_long": "42.346676,-71.097218",
            "city": "Boston",
            "state": "MA",
            "country": "USA",
            "location": "Boston, MA"
        },
        {
            "id": "12325",
            "name": "Another great show",
            "slug": "another-great-show",
            "date": "2017-10-14",
            "venue_name": "Open Space ",
            "lat_long": "40.438072,-3.679537",
            "city": "Madrid",
            "state": "",
            "country": "Spain",
            "location": "Madrid, Spain"
        }
]
```

### POST https://devapi.planckdata.com/pearljam-api/rsvp

Submit a dummy rsvp.

Response codes -
1. 200 - successful rsvp
1. 418 - I'm drinking tea. Please try again